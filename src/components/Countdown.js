import React, { useState, useEffect, useRef } from "react";
import { View, Text, StyleSheet } from "react-native";

const convertMinToMill = (min) => min * 1000 * 60;
const formatTime = (time) => (time < 10 ? `0${time}` : time);

const Countdown = ({ time = 1, isPlaying = true, setProgress }) => {
  const [milly, setMilly] = useState(convertMinToMill(time));
  const countRef = useRef(null);
  const minute = Math.floor(milly / 1000 / 60) % 60;
  const sec = Math.floor(milly / 1000) % 60;

  const countDown = () => {
    let timeLeft;
    setMilly((time) => {
      if (time === 0) {
        return time;
      }
      timeLeft = time - 1000;
      return timeLeft;
    });
    const tTime = convertMinToMill(time);
    setProgress(timeLeft/tTime);
  };

  useEffect(() => {
    if(!isPlaying) return;
    countRef.current = setInterval(countDown, 1000);
    return () => clearInterval(countRef.current);
  }, [isPlaying]);

  return (
    <View>
      <View style={styles.countdown}>
        <Text style={styles.text}>
          {formatTime(minute)}:{formatTime(sec)}
        </Text>
      </View>
      
    </View>
  );
};

const styles = StyleSheet.create({
  countdown: {
    backgroundColor: "rgba(94, 132, 226, 0.3)",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 10,
  },
  text: {
    color: "#FFFFFF",
    fontWeight: "bold",
    fontSize: 60,
  }
});

export default Countdown;
