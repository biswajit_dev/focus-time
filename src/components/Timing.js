import React from "react";
import { View, StyleSheet, TouchableOpacity, Text } from "react-native";

const Timing = ({ setTime, time }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={time === 5 ? styles.activeButton : styles.timingButton}
        onPress={() => setTime(5)}
      >
        <Text style={styles.text}>5</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={time === 10 ? styles.activeButton : styles.timingButton}
        onPress={() => setTime(10)}
      >
        <Text style={styles.text}>10</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={time === 15 ? styles.activeButton : styles.timingButton}
        onPress={() => setTime(15)}
      >
        <Text style={styles.text}>15</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={time === 20 ? styles.activeButton : styles.timingButton}
        onPress={() => setTime(20)}
      >
        <Text style={styles.text}>20</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: 20,
  },
  timingButton: {
    width: 40,
    height: 40,
    borderRadius: 50,
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#000000",
    justifyContent: "center",
  },
  activeButton: {
    backgroundColor: "#FFFFFF",
    borderColor: "#FFFFFF",
    justifyContent: "center",
    width: 40,
    height: 40,
    borderRadius: 50,
    alignItems: "center",
    borderWidth: 1,
  },
  text: {
    color: "#000000",
    fontSize: 18,
  },
});

export default Timing;
