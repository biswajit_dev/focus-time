import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";
import Focus from "./src/features/Focus";
import Timer from "./src/features/Timer";
import Timing from "./src/components/Timing";

export default function App() {
  const [focusSubject, setFocusSubject] = useState(null);
  const [time, setTime] = useState(5);
  
  return (
    <View style={styles.container}>
      {focusSubject ? (
        <Timer focusSubject={focusSubject} time={time} setFocusSubject={setFocusSubject} />
      ) : (
        <>
          <Focus addSubject={(text) => setFocusSubject(text)} />
          <Timing time={time} setTime={setTime} />
        </>
      )}

      {focusSubject && <Text>{focusSubject}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    backgroundColor: "#75E6DA",
  },
});
