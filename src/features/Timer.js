import React, { useState } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { ProgressBar } from "react-native-paper";
import Countdown from "../components/Countdown";

const Timer = ({ focusSubject, time, setFocusSubject }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [progress, setProgress] = useState(1);

  return (
    <View style={styles.container}>
      <ProgressBar color="#055C9D" style={{ height: 8 }} progress={progress} />
      <Countdown time={time} isPlaying={isPlaying} setProgress={setProgress} />
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => setIsPlaying(!isPlaying)}
        >
          <Text style={styles.buttonText}>{isPlaying ? "Pause" : "Play"}</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.focusText}>Focusing on</Text>
      <Text style={styles.focusTask}>{focusSubject}</Text>
      <TouchableOpacity style={styles.reset} onPress={() => setFocusSubject(null)}>
        <Text
          style={{
            color: "#FFFFFF",
          }}
        >
          Reset
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  focusText: {
    fontSize: 20,
    color: "#05445E",
    textAlign: "center",
    textDecorationLine: "underline",
    marginBottom: 10,
  },
  buttonContainer: {
    alignItems: "center",
  },
  buttonStyle: {
    width: 120,
    height: 60,
    borderRadius: 50,
    backgroundColor: "#003060",
    borderColor: "#003060",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#FFFFFF",
    fontSize: 20,
    fontWeight: "400",
  },
  focusTask: {
    fontSize: 16,
    color: "#274472",
    textAlign: "center",
    fontWeight: "bold",
  },
  reset: {
    position: "absolute",
    bottom: 20,
    backgroundColor: "#FF0000",
    marginLeft: 10,
    marginRight: 10,
    height: 50,
    borderRadius: 50,
    left: 50,
    padding: 15,
    width: 100,
    alignItems: "center"
  },
});

export default Timer;
